package golangtest_test

import (
	"errors"
)

type Memes struct {
	text  string
	order int32
}

func PrintFunnyText(order int32) (Memes, error) {
	memes := []Memes{
		Memes{
			text:  "Meme1 inee",
			order: 1,
		},
		Memes{
			text:  "Meme2",
			order: 2,
		},
	}

	for _, p := range memes {
		if p.order == order {
			return p, nil
		}
	}
	return Memes{}, errors.New("Meme not found with order")
}
